// Insulin Doses

var currentDoseM = document.getElementById('currentDoseM')
var currentDoseA = document.getElementById('currentDoseA')
var currentDoseE = document.getElementById('currentDoseE')
var currentDoseB = document.getElementById('currentDoseB')

var savedDoseM = document.getElementById('savedDoseM');
var savedDoseA = document.getElementById('savedDoseA');
var savedDoseE = document.getElementById('savedDoseE');
var savedDoseB = document.getElementById('savedDoseB');


  // Save the list to localStorage
function saveMyDose(){

  if (currentDoseM.value.length <1 && currentDoseA.value.length <1 && currentDoseE.value.length <1 && currentDoseB.value.length <1 ) return;

  savedDoseM.innerHTML = currentDoseM.value;
  savedDoseA.innerHTML = currentDoseA.value;
  savedDoseE.innerHTML = currentDoseE.value;
  savedDoseB.innerHTML = currentDoseB.value;

  // Clear input
  currentDoseM.value = '';
  currentDoseA.value = '';
  currentDoseE.value = '';
  currentDoseB.value = '';


  localStorage.setItem('savedDoseM', savedDoseM.innerHTML);
  localStorage.setItem('savedDoseA', savedDoseA.innerHTML);
  localStorage.setItem('savedDoseE', savedDoseE.innerHTML);
  localStorage.setItem('savedDoseB', savedDoseB.innerHTML);
}


// Check for saved wishlist items
var saved1 = localStorage.getItem('savedDoseM');
var saved2 = localStorage.getItem('savedDoseA');
var saved3 = localStorage.getItem('savedDoseE');
var saved4 = localStorage.getItem('savedDoseB');

// If there are any saved items, update our list
if (saved1, saved2, saved3, saved4) {
savedDoseM.innerHTML = saved1;
savedDoseA.innerHTML = saved2;
savedDoseE.innerHTML = saved3;
savedDoseB.innerHTML = saved4;
}



// (Glucose Readings)

var currentPreBfast = document.getElementById('currentPreBfast')
var currentPreLunch = document.getElementById('currentPreLunch')
var currentPreDinner = document.getElementById('currentPreDinner')
var currentPreBed = document.getElementById('currentPreBed')

var savedGlucoseM = document.getElementById('savedGlucoseM');
var savedGlucoseA = document.getElementById('savedGlucoseA');
var savedGlucoseE = document.getElementById('savedGlucoseE');
var savedGlucoseB = document.getElementById('savedGlucoseB');


  // Save the list to localStorage
function savePreBfast(){
  if (currentPreBfast.value.length <1)return;
  savedGlucoseM.innerHTML = currentPreBfast.value;
  currentPreBfast.value = '';
  localStorage.setItem('savedGlucoseM', savedGlucoseM.innerHTML);
}

function savePreLunch(){
  if (currentPreLunch.value.length <1)return;
  savedGlucoseA.innerHTML = currentPreLunch.value;
  currentPreLunch.value = '';
  localStorage.setItem('savedGlucoseA', savedGlucoseA.innerHTML);
}

function savePreDinner(){
  if (currentPreDinner.value.length <1)return;
  savedGlucoseE.innerHTML = currentPreDinner.value;
  currentPreDinner.value = '';
  localStorage.setItem('savedGlucoseE', savedGlucoseE.innerHTML);
}

function savePreBed(){
  if (currentPreBed.value.length <1)return;
  savedGlucoseB.innerHTML = currentPreBed.value;
  currentPreBed.value = '';
  localStorage.setItem('savedGlucoseB', savedGlucoseB.innerHTML);
}

// Check for saved wishlist items
var saved5 = localStorage.getItem('savedGlucoseM');
var saved6 = localStorage.getItem('savedGlucoseA');
var saved7 = localStorage.getItem('savedGlucoseE');
var saved8 = localStorage.getItem('savedGlucoseB');

// If there are any saved items, update our list

savedGlucoseM.innerHTML = saved5;
savedGlucoseA.innerHTML = saved6;
savedGlucoseE.innerHTML = saved7;
savedGlucoseB.innerHTML = saved8;


// My DiaryInput

var diaryInput = document.getElementById('diaryInput');
var myDiary = document.getElementById('myDiary');
function saveMyDiary(){
  if (diaryInput.value.length <1)return;
  myDiary.innerHTML += '<li>' + diaryInput.value + ' <button onclick="deleteItem(this)">Delete</delete>' + '</li>';
  diaryInput.value = '';
  localStorage.setItem('myDiary', myDiary.innerHTML);
}


function deleteItem(elementToDelete){
  elementToDelete.parentElement.remove()
  localStorage.setItem('myDiary', myDiary.innerHTML);
}

var saved9 = localStorage.getItem('myDiary');
myDiary.innerHTML = saved9;





function clearList(){
  localStorage.clear();
}


function refreshPage(){
  window.location.reload();
}
